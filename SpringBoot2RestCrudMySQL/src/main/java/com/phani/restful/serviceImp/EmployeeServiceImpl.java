package com.phani.restful.serviceImp;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phani.restful.model.Employee;
import com.phani.restful.repo.EmployeeRespository;
import com.phani.restful.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	EmployeeRespository employeeRespository;
	
	public Integer saveEmployee(Employee e) {
		Integer empId = employeeRespository.save(e).getEmpid();
		return empId;
	}

	
	public List<Employee> findAll() {
		List<Employee> empList = employeeRespository.findAll();
		return empList;
	}

	
	public Employee getEmployeeById(Integer id) {
		Optional<Employee> e = employeeRespository.findById(id);
		if(e.isPresent()) {
			return e.get();
		}
		return null;
	}

	
	public void deleteEmployeeById(Integer id) {
		employeeRespository.deleteById(id);
	}

	@Transactional
	public Integer updateEmployeeMail(Integer id, String email) {
		 return employeeRespository.updateEmployeeMail(id, email);
	}

	public Employee updateEmployeeInfo(Employee e) {
		Employee updatedEmpInfo = employeeRespository.save(e);
		return updatedEmpInfo;
	}

	

}
