package com.phani.restful.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "emp_tab")
public class Employee {
	
	@Id
	@GeneratedValue
	@Column(name = "emp_id")
	private Integer empid;
	
	@Column(name = "emp_name")
	private String ename;
	
	@Column(name = "emp_sal")
	private Double esal;
	
	@Column(name = "emp_email")
	private String email;
	
	@Column(name = "emp_hra")
	private Double hra;
	
	@Column(name = "emp_ta")
	private Double ta;

}
