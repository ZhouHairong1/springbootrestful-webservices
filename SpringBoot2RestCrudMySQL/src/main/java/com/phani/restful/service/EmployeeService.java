package com.phani.restful.service;

import java.util.List;

import com.phani.restful.model.Employee;


public interface EmployeeService {
	
	public abstract Integer saveEmployee(Employee e);
	public abstract List<Employee> findAll();
	public abstract Employee getEmployeeById(Integer id);
	public abstract void deleteEmployeeById(Integer id);
	public abstract Integer updateEmployeeMail(Integer id,String email);
	public abstract Employee updateEmployeeInfo(Employee e);
}
